package Solvers;

public enum SolversType {
    BACKTRACKING,
    BACKJUMPING,
    FORWARD_CHECKING
}
