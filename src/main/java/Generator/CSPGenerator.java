package Generator;

import Solvers.SolversUtils;
import Model.CSP.CSP;
import Model.CSP.Couple;
import Model.CSP.Vertex;

import java.util.*;

public class CSPGenerator {

    public final static String X = "X";

    public CSPGenerator() {}

    /**
     * Generate a CSP for our problem
     * @param nbVertices number of vertices at generate in the a CSP
     * @param domainSize the domain max
     * @param hardnessOfConstraints the hardness of constraints (the percentage of contraints that we take)
     * @param density the density (the percentage of links between our vertices)
     * @return A CSP of problem
     */
    public CSP generateCSP(int nbVertices, int domainSize, int hardnessOfConstraints, int density) {
        CSP csp = new CSP();
        // creation of vertices according to the number of vertices to be created and filling domains for each vertex
        List<Vertex> vertices = this.getVertices(nbVertices, domainSize);
        // we assign the list of vertices at our csp
        csp.setVertices(vertices);
        // Creation of constraints between each vertices
        csp.getConstraints().addAll(getConstraints(csp.getVertices(), hardnessOfConstraints, density));
        //todo à activer pour test
//        csp.getConstraints().add(new Couple(1, 3));
//        csp.getConstraints().add(new Couple(1, 4));
//        csp.getConstraints().add(new Couple(2, 4));
//        csp.getConstraints().add(new Couple(1, 5));
//
//        csp.getConstraints().add(new Couple(3, 7));
//        csp.getConstraints().add(new Couple(4, 7));
//        csp.getConstraints().add(new Couple(5, 7));
//
//        csp.getConstraints().add(new Couple(7, 8));
//        csp.getConstraints().add(new Couple(7, 9));
//        csp.getConstraints().add(new Couple(2, 8));
//        csp.getConstraints().add(new Couple(8, 11));
        // we create the links between each vertex based on the constraints created
        csp.creationLinksBetweenVertices();
        return csp;
    }

    private List<Vertex> getVertices(int nbVariable, int domainSizeMax) {
        List<Vertex> vertices = new ArrayList<>();
        Random random = new Random();
        int domainNumber = 1;
        for(int variable = 1; variable <= nbVariable; variable++) {
            Vertex vertex = new Vertex();
            vertex.setName(X + variable);
            vertex.setDomainMax(domainSizeMax);
            vertex.getLinkedVertices().put(vertex.getName(), new ArrayList<>()); // we init the map

            //todo à activer pour situation réel
             int domainSize = random.nextInt(vertex.getDomainMax()) + 1;
            //todo à activer pour test
//            int domainSize = 0;
//            if(variable == 1) {
//                domainSize = 2;
//            } else if(variable == 2) {
//                domainSize = 3;
//            } else if(variable == 3) {
//                domainSize = 2;
//            } else if(variable == 4) {
//                domainSize = 2;
//            } else if(variable == 5) {
//                domainSize = 2;
//            }
            for(int i = 0; i < domainSize; i++) {
                vertex.addDomain(domainNumber);
                domainNumber++;
            }

            vertices.add(vertex);
        }
        return vertices;
    }

    /**
     * Get constraints of CSP
     * @param vertices the vertices
     * @param hardnessOfConstraints the hardness of constraints (the percentage of contraints that we take)
     * @param density the density (the percentage of links between our vertices)
     * @return the list of couple of constraints
     */
    public static List<Couple> getConstraints(final List<Vertex> vertices, final int hardnessOfConstraints, final int density) {
        List<Couple> coupleValue = new ArrayList<>();
        Map<Vertex, List<Vertex>> couples = SolversUtils.getCouplesVertex(vertices);
        removeSomeLinksForRespectDensity(density, couples);
        for(Map.Entry<Vertex, List<Vertex>> couple : couples.entrySet()) {
            for(Vertex vertex : couple.getValue()) {
                coupleValue.addAll(getConstraints(couple.getKey(), vertex, hardnessOfConstraints));
            }
        }
        return coupleValue;
    }

    /**
     * Remove links between vertices to respect density
     * @param density the density (the percentage of links between our verti
     * @param couples the map couples, we allow to know the connections between each vertices
     */
    private static void removeSomeLinksForRespectDensity(double density, Map<Vertex, List<Vertex>> couples) {
        Random random = new Random();
        // we calculate the total arc number
        int arcSize = 0;
        for(Map.Entry<Vertex, List<Vertex>> couple : couples.entrySet()) {
            arcSize += couple.getValue().size();
        }

        //
        // Map def
        // Couples {
        //      {
        //          Key :
        //              Vertex {
        //                  name : X1
        //                  ...
        //              },
        //              List {
        //                  Vertex {
        //                      name : X2
        //                      ...
        //                  },
        //                  Vertex {
        //                      name: X3
        //                      ...
        //                  }
        //               }
        //
        //
        // With that, we know that X1 is linked to X2 and X3
        //

        int numberArcHasDelete = SolversUtils.roundNumber((100 - density) * ( (double) arcSize / 100));
        if(numberArcHasDelete != 0) {
            do {
                // We randomly choose the name of a key from our map
                List<String> keyName = new ArrayList<>();
                couples.forEach((key, vertice) -> keyName.add(key.getName()));
                String keyNameChoose = keyName.get(random.nextInt(couples.size()));
                // We get the vertices linked to this vertex (key name)
                Map.Entry<Vertex, List<Vertex>> vertexCouple  = couples.entrySet().stream().filter(vertexListEntry -> vertexListEntry.getKey().getName().equals(keyNameChoose)).findFirst().orElse(null);
                // In this list we will randomly choose a vertex to delete the arc
                int idxChoose = random.nextInt(vertexCouple.getValue().size());
                Vertex vertexChoose = vertexCouple.getValue().get(idxChoose);
                vertexCouple.getValue().remove(vertexChoose);
                // we decrease the number of arcs to remove
                numberArcHasDelete--;
                // we delete the association empty
                couples.entrySet().removeIf(entry -> entry.getValue().isEmpty());
            } while (numberArcHasDelete != 0);
        }
    }

    /**
     * We form all possible couples between vertex X and vertex Y and we take a certain percentage
     * @param vertexX vertex x
     * @param vertexY vertex y
     * @param hardnessOfConstraints the percentage to take
     * @return possible couples
     */
    private static List<Couple> getConstraints(final Vertex vertexX, final Vertex vertexY, final int hardnessOfConstraints) {
        List<Couple> couples = new ArrayList<>(SolversUtils.getValueCouples(vertexX, vertexY));
        List<Couple> finalCouples = new ArrayList<>();
        Random random = new Random();

        int numberOfCouplesToTake = SolversUtils.roundNumber(couples.size() * ( (double) hardnessOfConstraints / 100));
        if(numberOfCouplesToTake != 0) {
            do {
                Couple couple;
                do {
                    int choose = random.nextInt(couples.size());
                    couple = couples.get(choose);
                } while (finalCouples.contains(couple));
                finalCouples.add(couple);
                numberOfCouplesToTake--;
            } while (numberOfCouplesToTake != 0);
        }

        return finalCouples;
    }
}
