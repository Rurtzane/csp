import Solvers.*;
import Generator.CSPGenerator;
import Model.CSP.CSP;

import java.io.File;
import java.io.FileWriter;

public class Main {

    private double secondsBacktracking, secondsBackjumping, secondsForwardChecking;

    public static void main(String[] args) {
        CSP csp;
        int nbVariable, domainSize, hardnessOfConstraints, density;

        CSPGenerator cspGenerator = new CSPGenerator();
        Main main = new Main();

        if(args.length != 4) {
            System.out.println(MainMSG.ERROR_COMMAND);
            return;
        } else if(!main.isNumeric(args[0]) || !main.isNumeric(args[1]) || !main.isNumeric(args[2]) || !main.isNumeric(args[3])) {
            System.out.println(MainMSG.NOT_NUMBER);
            return;
        }
        nbVariable = Integer.parseInt(args[0]);
        domainSize = Integer.parseInt(args[1]);

        hardnessOfConstraints = Integer.parseInt(args[2]);
        if(hardnessOfConstraints < 0  || hardnessOfConstraints > 100) {
            System.out.println(MainMSG.ERROR_PERCENTAGE);
            return;
        }

        density = Integer.parseInt(args[3]);
        if(density < 0  || density > 100) {
            System.out.println(MainMSG.ERROR_PERCENTAGE);
            return;
        }

        csp = cspGenerator.generateCSP(nbVariable, domainSize, hardnessOfConstraints, density);
        try{
            File file=new File(MainMSG.RESULT_TXT);
            FileWriter fileWriter=new FileWriter(file);
            fileWriter.write(csp.toString());
            fileWriter.write(SolversUtils.lineBreak());
            fileWriter.write(main.executeAndDisplay(csp, SolversType.BACKTRACKING));
            fileWriter.write(main.executeAndDisplay(csp, SolversType.BACKJUMPING));
            fileWriter.write(main.executeAndDisplay(csp, SolversType.FORWARD_CHECKING));
            fileWriter.write(SolversUtils.lineBreak());
            fileWriter.write(main.writeTime(SolversType.BACKTRACKING, main.secondsBacktracking, csp.getSolutionBacktracking() != null));
            fileWriter.write(main.writeTime(SolversType.BACKJUMPING, main.secondsBackjumping, csp.getSolutionBackJumping() != null));
            fileWriter.write(main.writeTime(SolversType.FORWARD_CHECKING, main.secondsForwardChecking, csp.getSolutionForwardChecking() != null));
            fileWriter.write(SolversUtils.lineBreak());
            fileWriter.write(main.displaySolution(csp, SolversType.BACKTRACKING));
            fileWriter.write(main.displaySolution(csp, SolversType.BACKJUMPING));
            fileWriter.write(main.displaySolution(csp, SolversType.FORWARD_CHECKING));
            fileWriter.close();
            System.out.println(SolversUtils.lineBreak() + MainMSG.RESULT_AVAIBLE + MainMSG.RESULT_TXT);
        } catch (Exception ignored) { }
    }

    private String executeAndDisplay(CSP csp, SolversType solversType) {
        StringBuilder stringBuilder = new StringBuilder();
        long beginTime = getTimeMillis();
        switch (solversType) {
            case BACKTRACKING:
                stringBuilder.append(SolversUtils.lineBreak()).append(SolversType.BACKTRACKING).append(SolversUtils.lineBreak());
                Backtracking backtracking = new Backtracking();
                backtracking.execute(csp, stringBuilder);
                break;
            case BACKJUMPING:
                stringBuilder.append(SolversUtils.lineBreak()).append(SolversType.BACKJUMPING).append(SolversUtils.lineBreak());
                Backjumping backjumping = new Backjumping();
                backjumping.execute(csp, stringBuilder);
                break;
            case FORWARD_CHECKING:
                stringBuilder.append(SolversUtils.lineBreak()).append(SolversType.FORWARD_CHECKING).append(SolversUtils.lineBreak());
                ForwardChecking forwardChecking = new ForwardChecking();
                forwardChecking.execute(csp, stringBuilder);
                break;
        }
        long endTime = getTimeMillis();
        switch (solversType) {
            case BACKTRACKING:
                secondsBacktracking = getSeconds(beginTime, endTime);
                break;
            case BACKJUMPING:
                secondsBackjumping = getSeconds(beginTime, endTime);
                break;
            case FORWARD_CHECKING:
                secondsForwardChecking = getSeconds(beginTime, endTime);
                break;
        }
        return stringBuilder.toString();
    }

    private String writeTime(SolversType solversType, double seconds, boolean found) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak());
        stringBuilder.append(solversType.toString()).append(MainMSG.PERFORMED_IN).append(String.format("%.4f", seconds)).append(MainMSG.SECONDES).append(SolversUtils.lineBreak());
        if(found) {
            stringBuilder.append(solversType.toString()).append(MainMSG.SOLUTION_FOUND);
        } else {
            stringBuilder.append(solversType.toString()).append(MainMSG.SOLUTION_NOT_FOUND);
        }
        System.out.println(stringBuilder);
        stringBuilder.append(SolversUtils.lineBreak());
        return stringBuilder.toString();
    }

    private long getTimeMillis() {
        return System.currentTimeMillis();
    }

    private float getSeconds(long beginTime, long endTime) {
        return (endTime - beginTime) / 1000F;
    }

    private boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    private boolean isNumeric(CharSequence cs) {
        if (isEmpty(cs)) {
            return false;
        } else {
            int sz = cs.length();

            for(int i = 0; i < sz; ++i) {
                if (!Character.isDigit(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        }
    }

    private String displaySolution(CSP csp, SolversType solversType) {
        StringBuilder stringBuilder = new StringBuilder();
        switch (solversType) {
            case BACKTRACKING:
                if(csp.getSolutionBacktracking() != null) {
                    stringBuilder.append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak()).append(SolversType.BACKTRACKING).append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak());
                    stringBuilder.append(csp.getSolutionBacktracking().toString());
                }
                break;
            case BACKJUMPING:
                if(csp.getSolutionBackJumping() != null) {
                    stringBuilder.append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak()).append(SolversType.BACKJUMPING).append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak());
                    stringBuilder.append(csp.getSolutionBackJumping().toString());
                }
                break;
            case FORWARD_CHECKING:
                if(csp.getSolutionForwardChecking() != null) {
                    stringBuilder.append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak()).append(SolversType.FORWARD_CHECKING).append(SolversUtils.lineBreak()).append(SolversUtils.lineBreak());
                    stringBuilder.append(csp.getSolutionForwardChecking().toString());
                }
                break;
        }
        return stringBuilder.toString();
    }
}
