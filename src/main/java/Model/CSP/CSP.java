package Model.CSP;

import Model.Tree.Node;

import java.util.ArrayList;
import java.util.List;

public class CSP {

    private List<Vertex> vertices;
    private List<Couple> constraints;
    private Node solutionBacktracking;
    private Node solutionBackJumping;
    private Node solutionForwardChecking;

    public CSP() {
        constraints = new ArrayList<>();
    }

    /**
     * We retrieve the size of the domain for a specific vertex
     * @param name name of vertex research
     * @return the domains size
     */
    public int domainSizeByVertexName(String name) {
        Vertex found = vertices.stream().filter(vertex -> vertex.getName().equals(name)).findFirst().orElse(null);
        return found.getDomains().size();
    }

    /**
     * We retrieves the summit to which the domain belongs
     * @param domain the domain belonging to a vertex
     * @return the name of vertex
     */
    public String whoHasThisDomain(int domain) {
        Vertex found = vertices.stream().filter(vertex -> vertex.getDomains().contains(domain)).findFirst().orElse(null);
        return found.getName();
    }

    /**
     * we create the links between each vertex based on the constraints created
     */
    public void creationLinksBetweenVertices() {
        // for each couple (constraint)
        for(Couple couple : this.getConstraints()) {
            // We recover the vertex linked to the value X of the couple
            Vertex vertexX = this.getVertices().stream().filter(vertex -> vertex.getDomains().contains(couple.getX())).findFirst().orElse(null);
            // We recover the vertex linked to the value Y of the couple
            Vertex vertexY = this.getVertices().stream().filter(vertex -> vertex.getDomains().contains(couple.getY())).findFirst().orElse(null);
            // We check if the vertex X is already linked with the Y and check if the vertex Y is already linked with the X, if no, we link the vertice because there is an existing constraint for this couple
            boolean isTheLinkAlreadyExistingForVertexX = !vertexX.getLinkedVertices().get(vertexX.getName()).contains(vertexY.getName());
            boolean isTheLinkAlreadyExistingForVertexY = !vertexY.getLinkedVertices().get(vertexY.getName()).contains(vertexX.getName());
            if(isTheLinkAlreadyExistingForVertexX && isTheLinkAlreadyExistingForVertexY) {
                connectTwoVerticesTogether(vertexX, vertexY);
            }
        }
    }


    private void connectTwoVerticesTogether(Vertex vertexX, Vertex vertexY) {
        vertexX.getLinkedVertices().get(vertexX.getName()).add(vertexY.getName());
        vertexY.getLinkedVertices().get(vertexY.getName()).add(vertexX.getName());
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }

    public List<Couple> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<Couple> constraints) {
        this.constraints = constraints;
    }

    public Node getSolutionBacktracking() {
        return solutionBacktracking;
    }

    public void setSolutionBacktracking(Node solutionBacktracking) {
        this.solutionBacktracking = solutionBacktracking;
    }

    public Node getSolutionBackJumping() {
        return solutionBackJumping;
    }

    public void setSolutionBackJumping(Node solutionBackJumping) {
        this.solutionBackJumping = solutionBackJumping;
    }

    public Node getSolutionForwardChecking() {
        return solutionForwardChecking;
    }

    public void setSolutionForwardChecking(Node solutionForwardChecking) {
        this.solutionForwardChecking = solutionForwardChecking;
    }

    @Override
    public String toString() {
        return "CSP" +
                "\n  vertices=\n" + vertices.toString() + "," +
                "\n  constraints=\n" + constraints.toString() +
                "\n";
    }
}
