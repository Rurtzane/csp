package Model.CSP;

import java.util.*;

/**
 * A vertex of problem
 */
public class Vertex {

    /**
     * the name of vertex
     */
    private String name;

    /**
     * Their domains
     */
    private List<Integer> domains;

    /**
     * Map allowing to know if the vertice is linked to others
     */
    private Map<String, List<String>> linkedVertices;

    /**
     * The number of domain max that i can to create
     */
    private int domainMax;

    /**
     * Create vertex
     */
    public Vertex() {
        domains = new ArrayList<>();
        linkedVertices = new HashMap<>();
    }

    /**
     * Add a domain in the list
     * @param domain the domain
     */
    public void addDomain(Integer domain) {
        domains.add(domain);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getDomains() {
        return domains;
    }

    public Map<String, List<String>> getLinkedVertices() {
        return linkedVertices;
    }

    public void setLinkedVertices(Map<String, List<String>> linkedVertices) {
        this.linkedVertices = linkedVertices;
    }

    public int getDomainMax() {
        return domainMax;
    }

    public void setDomainMax(int domainMax) {
        this.domainMax = domainMax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return domainMax == vertex.domainMax &&
                Objects.equals(name, vertex.name) &&
                Objects.equals(domains, vertex.domains) &&
                Objects.equals(linkedVertices, vertex.linkedVertices);
    }

    @Override
    public String toString() {
        return "    " +name + " " + domains + " " +
                "\nlinkedVertices = " +
                linkedVertices + "\n";
    }
}