package Model.Tree;

import Solvers.SolversUtils;
import Model.CSP.Couple;
import Model.CSP.Vertex;

import java.util.*;

/**
 * Class representing a node of our tree
 */
public class Node {

    /**
     * Parent node
     */
    private Node parent;

    /**
     * The date of node
     */
    private Vertex data;

    /**
     * The sons of the node
     */
    private List<Node> children;

    /**
     * Let you know if the node has already been visited
     */
    private boolean isVisited;

    /**
     * Create node
     * @param name name of data
     */
    public Node(String name) {
        data = new Vertex();
        data.setName(name);
        children = new ArrayList<>();
    }

    /**
     * Create node
     * @param name name of data
     * @param linkedVertices map allowing to know if the vertice is linked to others
     * @param domain the domain of the data
     */
    private Node(String name, Map<String, List<String>> linkedVertices, Integer domain) {
        this(name);
        this.data.setLinkedVertices(linkedVertices);
        this.data.addDomain(domain);
    }

    /**
     * Add a child at the node
     * @param child children node
     */
    private void addChild(Node child) {
        child.setParent(this);
        this.children.add(child);
    }

    /**
     * Used to extend a node, each son will be represented by a different domain. What makes it possible to decompose the domain of the vertice in several nodes
     * @param vertex Vertex at extend
     */
    public void extend(Vertex vertex) {
        for (int domain : vertex.getDomains()) {
            Node child = new Node(vertex.getName(), vertex.getLinkedVertices(), domain);
            this.addChild(child);
        }
    }

    /**
     * Used to extend a node of algorithm forward checking, each son will be represented by a different domain. What makes it possible to decompose the domain of the vertice in several nodes if he respect the contraints
     * @param vertexToExtend the vertex to extend
     * @param constraints the constraints of csp
     * @param vertices the vertices of csp
     */
    public void extendForwardChecking(Vertex vertexToExtend, List<Couple> constraints, List<Vertex> vertices) {
        List<Vertex> verticesLinkedAtVertex = new ArrayList<>();
        for(Map.Entry<String, List<String>> vertexCouple : vertexToExtend.getLinkedVertices().entrySet()) {
            vertexCouple.getValue().forEach(nameVertexLinked -> verticesLinkedAtVertex.add(SolversUtils.findVertexByName(vertices, nameVertexLinked)));
        }
        
        for(int domain : vertexToExtend.getDomains()) {
            boolean path = false;
            for(Vertex vertexLinked : verticesLinkedAtVertex) {
                Vertex vertex = new Vertex();
                vertex.addDomain(domain);
                vertex.setName(vertexToExtend.getName());
                vertex.setLinkedVertices(vertexToExtend.getLinkedVertices());
                Map<String, List<Couple>> couples = SolversUtils.getValueCouples(Arrays.asList(vertex, vertexLinked));

                for(Map.Entry<String, List<Couple>> vertexCouple : couples.entrySet()) {
                    path = (vertexCouple.getValue().stream().filter(constraints::contains).count() >= 1);
                }
            }
            if(path) {
              Node child = new Node(vertexToExtend.getName(), vertexToExtend.getLinkedVertices(), domain);
              this.addChild(child);
            }
        }
    }

    public Node getParent() {
        return parent;
    }

    private void setParent(Node parent) {
        this.parent = parent;
    }

    public Vertex getData() {
        return data;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setData(Vertex data) {
        this.data = data;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    @Override
    public String toString() {
        return "Node{" +
                ", data=" + data +
                "parent=" + parent +
                '}';
    }
}
