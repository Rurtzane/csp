public interface MainMSG {

    String NOT_NUMBER = "Chiffres numériques uniquement";
    String ERROR_COMMAND = "Commande attendu : commande \"Nb variable (ex: 10) \" \"Taille du domaine (ex: 5) \" \"Dureté des contraintes en % (ex: 50) \" \"Densité en % (ex: 50) \"";
    String ERROR_PERCENTAGE = "Le pourcentage doit être compris entre 0 et 100";

    String RESULT_AVAIBLE = "Résultat disponible dans le fichier texte: ";
    String RESULT_TXT = "result.txt";

    String SOLUTION_NOT_FOUND = " n'a pas trouvé de solution";
    String SOLUTION_FOUND = " a trouvé une solution";

    String PERFORMED_IN = " effectuée en ";
    String SECONDES = " secondes.";
}
